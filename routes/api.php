<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//Route::get('teams', 'TeamController@getTeam');
//Route::post('addTeam', 'TeamController@addTeam');
Route::get('users/{id}', 'userController@getuserById');
Route::put('updateUser/{id}', 'userController@updateUser');
Route::delete('deleteUser/{id}', 'userController@deleteUser');
Route::post('sendPassword', 'userController@sendPassword');
Route::post('resetPassword', 'userController@process');
Route::post('handle', 'userController@handle');
Route::post('register', 'userController@register');
Route::post('login', 'userController@login');
Route::get('users', 'userController@getUser');
Route::put('addImage/{id}', 'userController@addImage');
Route::put('update/{id}', 'userController@update');
Route::put('adddescription/{id}', 'userController@adddescription');
Route::get('getdescription/{id}', 'userController@getdescription');
Route::put('updateimage/{id}', 'userController@updateimage');


Route::get('projects', 'ProjectController@getProject');
Route::post('addProject', 'ProjectController@addProject');
Route::delete('deleteProject/{projectId}', 'ProjectController@deleteProject');
Route::get('projects/{projectId}', 'ProjectController@getProjectById');
Route::put('updateProject/{projectId}', 'ProjectController@updateProject');

Route::get('tasks', 'TaskController@getTask');
Route::post('insertTask', 'TaskController@insertTask');
Route::delete('deleteTask/{TaskId}', 'TaskController@deleteTask');
Route::get('task/{taskId}', 'TaskController@getTaskById');
Route::put('updateTask/{taskId}', 'TaskController@updateTask');


Route::get('conges', 'CongeController@getConge');
Route::get('getDays/{user_id?}', 'CongeController@getDays');
Route::put('updateConge/{user_id}', 'CongeController@updateConge');
Route::get('getCongeById/{user_id}', 'CongeController@getCongeById');
Route::post('addConge', 'CongeController@addConge');
Route::delete('deleterequest/{id}', 'CongeController@deleterequest');
Route::put('updaterequest/{id}', 'CongeController@updaterequest');


Route::get('roles', 'roleController@getRole');
Route::get('getchart/{id}', 'roleController@getchart');
Route::post('addRole', 'roleController@addRole');
Route::get('techleadprojects/{assignedTo}', 'roleController@techleadprojects');



Route::get('events', 'eventController@getEvent');
Route::post('insertEvent', 'eventController@insertevent');
Route::delete('deleteEvent/{id}', 'EventController@deleteEvent');
Route::get('events/{id}', 'EventController@getEventById');
Route::put('updateEvent/{id}', 'EventController@updateEvent');
Route::post('eventadd', 'eventController@eventadd');
Route::get('geteventbyuser/{userid}', 'eventController@geteventbyuser');


Route::get('cras', 'craController@getCra');
Route::post('insertCra', 'craController@insertcra');
Route::delete('deleteCra/{id}', 'craController@deleteCra');
Route::get('cra/{userid}', 'CraController@getCraById');
Route::put('updateCra/{id}', 'CraController@updateCra');
Route::post('addcra', 'craController@addcra');
Route::get('databyweek/{id}', 'craController@databyweek');
Route::post('addfunction', 'craController@addfunction');
Route::get('groupcrabyweek/{id}', 'craController@groupcrabyweek');
Route::get('getlist/{id}', 'craController@getlist');
Route::get('getcrabyuser/{id}', 'craController@getcrabyuser');





Route::get('tests', 'roleController@getTestrole');
Route::post('addTestrole', 'roleController@addTestrole');
Route::delete('deleteRole/{taskId}', 'roleController@deleteRole');
Route::put('updateRole/{taskId}', 'roleController@updateRole');
Route::get('tests/{taskId}', 'roleController@gettestById');
Route::get('getprojectbyname/{id}', 'roleController@getprojectbyname');
Route::get('getOnlyName', 'projectController@getOnlyName');
Route::get('getNameById/{project_id}', 'roleController@getNameById');
Route::get('indexShortcode', 'roleController@indexShortcode');
Route::get('getTaskByProject/{project_id}/{user_id}', 'roleController@getTaskByProject');
Route::get('getTaskithoutProject/{user_id}', 'roleController@getTaskithoutProject');
Route::get('getalltasks/{project_id}', 'roleController@getalltasks');
Route::get('getstasproject', 'projectController@getstasproject');


Route::get('getUserByProject/{id}', 'projectController@getUserByProject');
Route::get('getSum', 'projectController@getSum');
Route::get('getprojectperuser/{id}', 'projectController@getprojectperuser');

Route::put('insert/{assignedTo}', 'userController@insert');
Route::put('updatedatevalue/{status}', 'roleController@updatedatevalue');

Route::post('patchimage/{id}', 'userController@patchimage');



Route::get('getloutdate', 'userController@getloutdate');
Route::get('getdate/{user_id}', 'roleController@getdate');
Route::get('getstat', 'roleController@getstat');
Route::get('getstatus', 'projectController@getstatus');
Route::get('gettasksforuser', 'roleController@gettasksforuser');


Route::get('tasktitle', 'roleController@tasktitle');
Route::get('getprojecttitle', 'projectController@getprojecttitle');
Route::get('getprojectstatus', 'projectController@getprojectstatus');




Route::get('getNotificationByUser/{notifiable}', 'roleController@getNotificationByUser');
Route::get('getCongeNotificationByUser/{notifiable}', 'roleController@getCongeNotificationByUser');
Route::get('getRoleNotificationByUser/{notifiable}', 'roleController@getRoleNotificationByUser');
Route::get('readnotification/{notifiable}', 'roleController@readnotification');
Route::get('unreadnotificationcount/{notifiable}', 'roleController@unreadnotificationcount');
Route::get('getproductivitybyuser/{id}', 'roleController@getproductivitybyuser');

Route::get('displaydata/{id}', 'roleController@displaydata');
Route::get('getdatastatus/{id}', 'roleController@getdatastatus');
Route::get('stattask/{id}', 'roleController@stattask');

Route::post('message', 'ChatController@message');
Route::post('sendMail', 'CongeController@sendMail');

Route::get('getprojectuserdata/{id}', 'roleController@getprojectuserdata');

Route::get('gettaskproject/{projectid}/{user_id}', 'craController@gettaskproject');
Route::get('projectlistperuser/{user_id}', 'craController@projectlistperuser');
Route::get('week/{id}', 'craController@week');
Route::get('thisweek/{id}', 'craController@thisweek');
Route::get('userweek/{id}/{startofweek}', 'craController@userweek');
Route::get('weeknumber', 'craController@weeknumber');
 
Route::get('specproject/{user_id}', 'roleController@specproject');

Route::post('documentadd', 'documentController@documentadd');
Route::get('getdocument/{id}', 'documentController@getdocument');
Route::get('techleadview/{assignedTo}/{project_id}', 'roleController@techleadview');
Route::post('uploadimage/{id}', 'documentController@uploadimage');
Route::get('getimageuser/{id}', 'documentController@getimageuser');
Route::get('testlist/{user_id}', 'craController@testlist');
Route::get('image/{filename}', 'documentController@displayImage')->name('image.displayImage');
Route::get('images', 'documentController@displayAllmages')->name('images.displayAllmages');

//link
Route::get('links', 'documentController@getLink');
Route::post('insertlink', 'documentController@insertLink');
Route::delete('deletelink/{id}', 'documentController@deletelink');
Route::get('link/{id}', 'documentController@getLinkById');
