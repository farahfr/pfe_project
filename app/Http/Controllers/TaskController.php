<?php

namespace App\Http\Controllers;

use App\erpw_task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function getTask()
    {
        return response()->json(erpw_task::all(), 200);
    }
    public function insertTask(Request $request)
    {
        $test = erpw_task::create($request->all());
        return response($test, 201);
    }
    public function getTaskById($id)
    {
        $task = erpw_task::find($id);
        if (is_null($task)) {
            return response()->json(['message' => 'Ouups!'], 404);
        }
        return response()->json($task::find($id), 200);
    }
    public function updateTask(Request $request, $id) {
        $team = erpw_task::find($id);
        if(is_null($team)) {
            return response()->json(['message' => 'Not Found'], 404);
        }
        $team->update($request->all());
        return response($team, 200);
    }
    public function deleteTask(Request $request, $id) {
        $team = erpw_task::find($id);
        if(is_null($team)) {
            return response()->json(['message' => 'Not Found'], 404);
        }
        $team->delete();
        return response()->json(null, 204);
    }
}
