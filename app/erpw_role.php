<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\erpw_user;
class erpw_role extends Model
{
    public function users(){
        return $this->belongsToMany('App\erpw_user');
    }
    
}
