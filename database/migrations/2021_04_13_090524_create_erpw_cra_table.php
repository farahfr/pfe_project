<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErpwCraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erpw_cras', function(Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('days');
            $table->string('status')->nullable();
            $table->integer('userid')->index()->unsigned();
            $table->integer('projectid')->index()->unsigned();
            $table->string('taskid');
            $table->string('description')->nullable();
            $table->string('startofweek')->nullable();
            $table->string('endofweek')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
