@component('mail::message')
# Change Password Request

Click On Button Below To Change Password.
@component('mail::button', ['url' => 'http://localhost:4200/pages/response-reset?token='.$token])
    Reset Password
@endcomponent

Thanks,<br>
ERP_WELYNE
@endcomponent
