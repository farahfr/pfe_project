<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErpwDatesavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erpw_datesaves', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->datetime('lastsigninat')->nullable();
            $table->string('last_login_ip')->nullable();
            $table->datetime('last_logout')->nullable();
            $table->integer('assignedTo')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datesaves');
    }
}
