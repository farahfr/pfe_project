<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\erpw_role;
use App\erpw_projects;
use App\erpw_datesave;
class erpw_user extends Authenticatable implements JWTSubject
{
    use Notifiable;
    public function project(){
        return $this->belongsTo('App\erpw_projects');
    }
    public function user() {
        return $this->belongsTo(erpw_user::class , 'assignedTo');
      }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'lastname',  'birthdate', 'phonenumber', 'email', 'experience' ,'password', 'image' ,'isTechlead', 'description', 'role' , 'data'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function roles(){
        return $this->belongsToMany('App\erpw_role');
    }

    public function proj(){
        return $this->belongsToMany('App\erpw_project');
    }
    public function datesave(){
        return $this->belongsTo('App\erpw_datesave');
    }

    public function tasks()
    {
        return $this->morphMany('App\erpw_task', 'taskassigned');
    }
    public function getId()
{
  return $this->id;
}
}
