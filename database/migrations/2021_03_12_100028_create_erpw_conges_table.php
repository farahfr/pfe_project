<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateErpwCongesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('erpw_conges', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->date('datedebut');
			$table->string('status')->default('Being Processed');
			$table->string('Description')->nullable();
			//$table->enum('status', ['accepted', 'rejected', 'being processedbeing processed'])->default('being processed');
           // $table->enum('type', ['Bereavement', 'Paid_Sabbatical', 'Unpaid_Sabbatical', 'Maternity_leave', 'Medical_leave']);
			$table->string('type');
            $table->date('datefin');
			$table->integer('nbr')->nullable();
			$table->integer('user_id');
			$table->integer('remainingdays')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('conges');
	}

}
