<?php

namespace App\Http\Controllers;

use App\erpw_role;
use Illuminate\Http\Request;
use App\erpw_role_user;
use App\erpw_project_user;
use App\erpw_projects;
use DB;
use App\erpw_notification;
use App\erpw_user;
use App\Notifications\RoleNotification;
use Illuminate\Support\Facades\Notification;
use Illuminate\Notifications\DatabaseNotification;

class roleController extends Controller
{
    public function getRole()
    {
        return response()->json(erpw_role::all(), 200);
    }
    public function addRole(Request $request)
    {   
        $test = erpw_role_user::create($request->all());
        return response($test, 201);

       
    }
    public function getTestrole()
    {

        return 
        $tasks = erpw_project_user::with('project')->get();
        $project_id->project->projectTitle;



        //return response()->json(erpw_project_user::all(), 200);
       
    }
    public function addTestrole(Request $request)
    {   
        //$user = erpw_user::all();
        $user = erpw_user::find($request->user_id);
         $test = erpw_project_user::create($request->all());
        Notification::send($user , new RoleNotification($request->roleenum));

        return response($test, 201);
    }
    // $user = erpw_user::all()->where('user_id',"=",'id');
    // $test = erpw_project_user::create($request->all());
    // Notification::send($user , new RoleNotification($request->roleenum));
    // return response($test, 201);
    public function getTestById($id)
    {
        $team = erpw_project_user::find($id);
        if (is_null($team)) {
            return response()->json(['message' => 'Ouups!'], 404);
        }
        return response()->json($team::find($id), 200);
    }

   
  
    public function deleteRole(Request $request, $taskId) {
        $evnt = erpw_project_user::find($taskId);
        if(is_null($evnt)) {
            return response()->json(['message' => 'Not Found'], 404);
        }
        $evnt->delete();
        return response()->json(null, 204);
    }
   
    public function updateRole(Request $request, $taskId) {
        $evnt = erpw_project_user::find($taskId);
        if(is_null($evnt)) {
            return response()->json(['message' => 'Not Found'], 404);
        }
        $evnt->update($request->all());
        return response($evnt, 200);
    }


    public function getNameById($project_id)
    {
        $team = erpw_projects::where('projectId',"=",$project_id)->pluck('projectTitle')->first();
        if (is_null($team)) {
            return response()->json(['message' => 'Ouups!'], 404);
        }
        return response()->json($team, 200);

    }
   
 

    public function getTaskByProject($project_id , $user_id){
        $test = erpw_project_user::where('project_id',"=",$project_id)->where('user_id',"=",$user_id)->get();
        if(is_null($test)){
            return response()->json(['message' => 'Oups'], 404);
        }
        return response()->json($test,200);
    }
    public function getTaskithoutProject($user_id){
        $test = erpw_project_user::where('user_id',"=",$user_id)->get();
        if(is_null($test)){
            return response()->json(['message' => 'Oups'], 404);
        }
        return response()->json($test,200);
    }
  
    public function updatedatevalue(Request $request, $status) {
   
        DB::table('erpw_project_users')
        ->where('status' ,'Completed')
            ->update([
                'completeddate'    => date('Y-m-d H:i:s')
            ]);

    }
    public function getdate($id)
    {
        return $team=  DB::table('erpw_project_users')->where('user_id',"=",$id)
        ->whereDate('releasedate', '>=', 'completeddate')->get()  ;
            
    }






//nbre of completed tasks' per user
public function getstat()
{ 
    $team=  DB::table('erpw_project_users')
    ->whereColumn('releasedate','>=','completeddate')
    ->count();    

    $total=  DB::table('erpw_project_users')
    ->count();
    return response()->json(array($team , $total));
       
     
}

public function getproductivitybyuser($id)
{ 

    $team=  DB::table('erpw_project_users')
    ->whereColumn('releasedate','>=','completeddate')->where('user_id',"=",$id)
    ->count();    

    $new=  DB::table('erpw_project_users')->where('user_id',"=",$id)->where('status',"=", 'New')
    ->count();
     
    $lost=  DB::table('erpw_project_users')
    ->whereColumn('releasedate','<','completeddate')->where('user_id',"=",$id)
    ->count(); 
    return response()->json(array($team , $new , $lost));

     
}


//all tasks per user
public function gettasksforuser()
{ 
    $team=  DB::table('erpw_project_users')
    ->count();    

    return response()->json($team, 200);
       
     
}




public function tasktitle()
{ 
    $team=  DB::table('erpw_project_users')
    ->where('releasedate','>=','2021-06-28')
    ->pluck('taskTitle');    

    return response()->json($team, 200);
       
     
}

public function getNotificationByUser($notifiable)
    {
        $user = erpw_user::find($notifiable);
        foreach($user ->notifications as $notifiable) {
            //echo $notifiable->type;
           // echo $notifiable->type;
         return response()->json($user->notifications, 200);
        // return response()->json(array($notifiable->data), 200);
        // return response()->json(($notifiable->data), 200);

    }
}

public function getCongeNotificationByUser($notifiable)
    {
        //$user = erpw_user::find($notifiable);

  $ts =     
        DB::table('erpw_notifications')->where('type', 'App\Notifications\CongeNotification')
        ->where('notifiable_id',"=",$notifiable)
        ->get();   
    // $ts = DB::table('notifications')->where('notifiable_id',"=",$notifiable)->where('type', 'App\Notifications\CongeNotification')->get();
     return response()->json($ts , 200);

}
public function getRoleNotificationByUser($notifiable)
    {
        // $user = erpw_user::find($notifiable);

           
        // foreach($user ->notifications as $notifiable) {
        //      $user
        //      ->notifications
        //      ->where('type', 'App\Notifications\RoleNotification')
        //      ->where('notifiable_id',"=",$notifiable)
        //      ->all();  
        // }

        // return response()->json($user->notifications, 200);
        $ts =     
        DB::table('erpw_notifications')->where('type', 'App\Notifications\RoleNotification')
        ->where('notifiable_id',"=",$notifiable)
        ->get();   
     return response()->json($ts , 200);

}

//App\Notifications\CongeNotification

        //read notification
        public function readnotification($notifiable)
        {
            $user = erpw_user::find($notifiable);

            foreach ($user->unreadNotifications as $notification) {
                $notification->markAsRead();
            }
        }
    

    public function unreadnotificationcount($notifiable){
        $user = erpw_user::find($notifiable);
        foreach ($user->unreadNotifications as $notification) {
            return $user->unreadNotifications->count();
    
        }
    
    }

    public function displaydata($id){
     
        return  $project=  erpw_project_user::with('project')
        ->select('project_id')->where('user_id',"=",$id)->distinct()
        ->get();
        $project_id->project->projectTitle;

    }

    public function getdatastatus($id){
   
    $data = erpw_project_user::with('project')->where('user_id',"=",$id)
    
    ->get();
    
    $attrs = [];
    if (is_array($data) || is_object($data)) {
    foreach ($data as $key => $taskId) {
      
        $attrs[$taskId->project_id][] = $taskId->status;

    }
    }
    return response()->json(array($attrs));
    }



    public function stattask($id)
{ 

    $new=  DB::table('erpw_project_users')
    ->where('status','=','New')->where('user_id',"=",$id)->count();
    

    $inprocess=  DB::table('erpw_project_users')->where('user_id',"=",$id)
    ->where('status',"=", 'In-Process')
    ->count();

    $pending=  DB::table('erpw_project_users')->where('user_id',"=",$id)
    ->where('status',"=", 'Pending')
    ->count();

    $completed=  DB::table('erpw_project_users')->where('user_id',"=",$id)
    ->where('status',"=", 'Completed')
    ->count();
    return response()->json(array($new , $inprocess , $pending, $completed));

     
}

public function getprojectuserdata($id)
{
    $test = erpw_project_user::with('project')->where('user_id',"=",$id)->select('project_id')->distinct()->get();
    if(is_null($test)){
        return response()->json(['message' => 'Oups'], 404);
    }
   
    return response()->json($test,200);
   
}

public function getalltasks($project_id){
    $test = erpw_project_user::with('user')->with('project')->where('project_id',"=",$project_id)->get();
    if(is_null($test)){
        return response()->json(['message' => 'Oups'], 404);
    }
    return response()->json($test,200);
}


public function specproject($user_id){
    $test = erpw_project_user::with('project')->with('user')->where('user_id',"=",$user_id)
    ->groupBy('project_id')
    ->get();
    if ($test->isEmpty()) {
        $test['status'] = 0;

        $test['message'] = 'There is no data';

        //return response()->json($response,200);
    }
    
    return response()->json($test,200);
}

//get list of project wehre user is techlead
public function techleadprojects($assignedTo){
   return $techlead = erpw_projects::where('assignedTo',"=",$assignedTo)->get();
}

//all tasks by project where user is techlead
public function techleadview($assignedTo, $project_id){
     $techlead = erpw_projects::where('assignedTo',"=",$assignedTo)->where('projectId',"=",$project_id)->pluck('projectId');
     return $techleadtasks = erpw_project_user::where('project_id',"=",$techlead)->with('user')->with('project')->get();

}


public function getchart($id){
        $project=  erpw_project_user::get();
        
        $taskstatus=  DB::table('erpw_project_users')
        ->selectRaw('taskId')->where('user_id',"=",$id)->where('status',"=", 'New')->groupBy('project_id')->count()
        ;
        $taskstatus1=  DB::table('erpw_project_users')->selectRaw('taskId')
       ->where('user_id',"=",$id)->where('status',"=",'In-Process')        
        ->groupBy('project_id')->count();
        $taskstatus2=  DB::table('erpw_project_users')->selectRaw('taskId')
        ->where('user_id',"=",$id)->where('status',"=",'Pending')
        ->groupBy('project_id')->count();
        $taskstatus3=  DB::table('erpw_project_users')->selectRaw('taskId')
       ->where('user_id',"=",$id)->where('status',"=",'Completed')
        ->groupBy('project_id')->count();
        if(($taskstatus == 0 ) && ($taskstatus1 == 0) && ($taskstatus2 == 0) && ($taskstatus3== 0) ) {
            return response()->json(array(['message' => 'There Is No Data!']), 404);
        }
    return response()->json(array($taskstatus,$taskstatus1,$taskstatus2,$taskstatus3));
}
}