<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Model;
use App\erpw_project_user;
use App\erpw_projects;
use App\erpw_user;
class erpw_project_user extends Authenticatable implements JWTSubject
{
    use Notifiable ;
    public $timestamps = false;

    public function projectuser(){
        return $this->belongsTo('App\erpw_project_user');
    }
    public function project() {
        return $this->belongsTo(erpw_projects::class , 'project_id');
      }
      public function user() {
        return $this->belongsTo(erpw_user::class , 'user_id');
      }
      public function techlead() {
        return $this->belongsTo(erpw_user::class , 'assignedTo');
      }

    protected $fillable = [
        'project_id', 'user_id', 'roleenum','taskTitle', 'taskMessage',  'assignedTo', 'createdOn', 'releasedate', 'status','completeddate',
    ];
    protected $primaryKey = 'taskId';
 





   /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

}
