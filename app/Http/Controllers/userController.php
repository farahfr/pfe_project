<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Mail\ResetPasswordMail;
use App\erpw_user;
use App\erpw_role;
use App\erpw_datesave;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTExceptions;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Routing\RouteCollection;
use Auth;

class userController extends Controller
{
    public function register(Request $request)
    {
        $user = erpw_user::Where('email', $request['email'])->first();
        if($user){
            $response['status'] = 0;
            $response['message'] = 'Email already Exists';
            $response['code'] = 409;
        }
        else {
            $user = erpw_user::create([
                'name'      => $request->name,
                'email'     => $request->email,
                'password'  => bcrypt($request->password),
                'lastname'       =>$request->lastname,
                'birthdate'      => $request->birthdate,
                'email'          => $request->email,
                'phonenumber'    => $request->phonenumber,
                'experience' => $request->experience,
               // 'image'          => $request->image,
                'roleenum'          => $request->roleenum,

            ]);
            // $user = new erpw_user ;
            // $user->email = $request->email;
            // $user->password = bcrypt($request->password);
            // $user->name = $request->name;
            // $user->lastname = $request->lastname;
            // $user->birthdate = $request->birthdate;
            // $user->phonenumber = $request->phonenumber;
            // $user->latestactivity = $request->latestactivity;
            // $user->hiringdate = $request->hiringdate;
            // $user->image = $request->image;
            // $user->roleenum = $request->roleenum;
            // $user->save();
            // $data = array('status'=>$user);
            $data = [
            
                'email' => $request->email,
                'password'=> $request->password,
                'name'=> $request->name,
                'lastname'=> $request->lastname,
                'password'=> $request->password,

        
              ];
              Mail::send('register', $data, function($message) use ($data) {
                $message->to($data['email'])
                ->subject('Welcome');
              });
            // if($request->hasfile('image')){
            //     $completeFileName= $request->file('image')->getClientOriginalName();
            //             //return dd($completeFileName);
                
            // }
            $response['status'] = 1;
            $response['message'] = 'User Registered Successfully';
            $response['code'] = 200;
        }
       

        return response()->json($response);
    }
  /*
*/public function login(Request $request)
{    
    $credentials = $request->only('email' , 'password');
  
    try{
        if (!JWTAuth::attempt($credentials)) {

            $response['status'] = 0;
            $response['data'] = null ;
            $response['message'] = 'Email or Password is incorrect';
            $response['code'] = 401;
            return response()->json($response);

        }
    }catch (JWTException $e) {
        $response['data'] = null ;
        $response['code'] = 500;
        $response['message'] = 'Could Not Create Token';
        return response()->json($response);

    }
    //$user = auth()->user();
    $project_user = DB::table('erpw_project_users')->select('roleenum')->where('user_id','=',auth()->user()->id)->get()->first();
    $user = auth()->user();
    $user_Admin = DB::table('erpw_users')->where('id','=',auth()->user()->id)->where('role','Super_Admin')->get()->first();
    $user_Guest = DB::table('erpw_users')->where('id','=',auth()->user()->id)->where('role','Guest')->get()->first();

    if (! $project_user && $user_Admin)
{
    $data['token'] = auth()->claims([
        'user_id' =>$user->id,
        'email'=> $user->email,
        'role'=>$user->role,
        'default'=>$user->default,
        //'role'=>$user_Guest->role,
        'isTechlead'=>$user->isTechlead
        //'roleenum'=> $project_user->roleenum,
    ])->attempt($credentials);
}

if (! $project_user && $user_Guest)
{
    $data['token'] = auth()->claims([
        'user_id' =>$user->id,
        'email'=> $user->email,
        'default'=>$user->default,
        //'role'=>$user_Guest->role,
        'isTechlead'=>$user->isTechlead
        //'roleenum'=> $project_user->roleenum,
    ])->attempt($credentials);
}
    
if ($project_user && $user_Admin)
 {
        $data['token'] = auth()->claims([
            'user_id' =>$user->id,
            'email'=> $user->email,
            'role'=>$user->role,
            'roleenum'=> $project_user->roleenum,
            'isTechlead'=>$user->isTechlead,
        'default'=>$user->default,

        ])->attempt($credentials);
    }
    if ($project_user && ! $user_Admin)
 {
        $data['token'] = auth()->claims([
            'user_id' =>$user->id,
            'email'=> $user->email,
            'default'=>$user->default,
            'roleenum'=> $project_user->roleenum,
            'isTechlead'=>$user->isTechlead
        ])->attempt($credentials);
    }
   
    if (! $project_user && $user_Admin){
    $response['data'] = $data ;
    $response['status'] = 1;
    $response['message'] = 'Login Successfully';
    $response['code'] = 200;
    $response['name'] = $user->name;
    $response['lastname'] = $user->lastname;
    $response['email'] = $user->email;
    $response['birthdate'] = $user->birthdate;
    $response['hiringdate'] = $user->hiringdate;
    $response['phonenumber'] = $user->phonenumber;
    //$response['roleenum'] = $project_user->roleenum;
    $response['role'] = $user->role;
    $response['id'] = $user->id;
    $response['isTechlead'] = $user->isTechlead;
    $response['default'] = $user->default;
    $response['experience'] = $user->experience;

    }
    if (! $project_user && $user_Guest) {
        $response['data'] = $data ;
    $response['status'] = 1;
    $response['message'] = 'Login Successfully';
    $response['code'] = 200;
    $response['name'] = $user->name;
    $response['lastname'] = $user->lastname;
    $response['email'] = $user->email;
    $response['birthdate'] = $user->birthdate;
    $response['hiringdate'] = $user->hiringdate;
    $response['phonenumber'] = $user->phonenumber;
    $response['default'] = $user->default;
    $response['isTechlead'] = $user->isTechlead;
    $response['experience'] = $user->experience;

    //$response['role'] = $user->role;
    $response['id'] = $user->id;
    }
    if ($project_user && $user_Admin) {
        $response['data'] = $data ;
    $response['status'] = 1;
    $response['message'] = 'Login Successfully';
    $response['code'] = 200;
    $response['name'] = $user->name;
    $response['lastname'] = $user->lastname;
    $response['email'] = $user->email;
    $response['birthdate'] = $user->birthdate;
    $response['hiringdate'] = $user->hiringdate;
    $response['phonenumber'] = $user->phonenumber;
    $response['default'] = $user->default;
    $response['isTechlead'] = $user->isTechlead;
    $response['role'] = $user->role;
    $response['id'] = $user->id;
    $response['experience'] = $user->experience;

    }
    if ($project_user && !$user_Admin) {
        $response['data'] = $data ;
    $response['status'] = 1;
    $response['message'] = 'Login Successfully';
    $response['code'] = 200;
    $response['name'] = $user->name;
    $response['lastname'] = $user->lastname;
    $response['email'] = $user->email;
    $response['birthdate'] = $user->birthdate;
    $response['hiringdate'] = $user->hiringdate;
    $response['phonenumber'] = $user->phonenumber;
    $response['default'] = $user->default;
    $response['isTechlead'] = $user->isTechlead;
    //$response['role'] = $user->role;
    $response['id'] = $user->id;
    $response['experience'] = $user->experience;

    }
    
    $user = erpw_datesave::create([
        
        'lastsigninat'  =>Carbon::now()->toDateTimeString(),
        'last_login_ip' => $request->getClientIp(),
        //'last_logout'   =>Carbon::now()->toDateTimeString(),
        'assignedTo' => $user->id,
    ]);
    

    return response()->json($response);
  
}
public function insert(Request $request , $assignedTo)
{
        $user = auth()->user();
        DB::table('erpw_datesaves')
        ->orderBy('created_at', 'DESC')->where('assignedTo',$assignedTo)
        ->limit(1)

            ->update([
                'last_logout'    => date('Y-m-d H:i:s')
            ]);
 }
    public function sendPassword(Request $request) {
    //return $request->all();
        if(!$this->ValidateEmail($request->email)){
            return $this->failedResponse();
        }
        $this->send($request->email);
        return $this->successResponse();

    }


    public function send($email){
    $token = $this->createToken($email);
    Mail::to($email)->send(new ResetPasswordMail($token));

    }

public function createToken($email) {
    $oldToken = DB::table('erpw_password_resets')->where('email', $email)->first();
    if($oldToken){
    return $oldToken->token;
    }
     $token = str_random(60);
     $this->saveToken($token, $email);
     return $token;
}

public function saveToken($token, $email){
DB::table('erpw_password_resets')->insert( [
    'email' =>$email,
    'token' =>$token,
    'created_at' => Carbon::now()
    ]);

}


    public function ValidateEmail($email) {
    return !!erpw_user::where('email' , $email)->first();
    }
    public function failedResponse(){
    return response()->json([
        'error' => 'Email does not exist'
    ]

    );
    }

    public function successResponse(){
         return response()->json([
        'data' => 'Reset email is send successfully'
    ]) ;
    }


    public function process(ChangePasswordRequest $request){
         return $this->getPasswordResetTableRow($request)->count() > 0 ? $this->ChangePassword($request) : $this->tokenNotFoundResponse();
    }

    private function getPasswordResetTableRow($request) {
         return DB::table('erpw_password_resets')->where(['email' => $request->email, 'token'=>$request->resetToken]);
    }

    public function tokenNotFoundResponse(){
        return response()->json([
            'error' => 'Token or Email is incorrect'
        ] , \Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY) ;
    }

    private function ChangePassword($request){
        $user = erpw_user::whereEmail($request->email)->first();
        $user->update(['password' =>bcrypt($request->password)]) ;
        $this->getPasswordResetTableRow($request)->delete();
        return response()->json(['password successfully change'] ,
            \Symfony\Component\HttpFoundation\Response::HTTP_CREATED);

    }

    public function getUser()
    {
        return response()->json(erpw_user::all(), 200);
    }
    public function getUserById($id)
    {
        $team = erpw_user::find($id);
        if (is_null($team)) {
            return response()->json(['message' => 'Ouups!'], 404);
        }
        return response()->json($team::find($id), 200);
    }
    public function updateUser(Request $request, $id) {
        $team = erpw_user::find($id);
        if(is_null($team)) {
            return response()->json(['message' => 'Not Found'], 404);
        }
        $team->update($request->all());
        return response($team, 200);
    }
    public function deleteUser(Request $request, $id) {
        $team = erpw_user::find($id);
        if(is_null($team)) {
            return response()->json(['message' => 'Not Found'], 404);
        }
        $team->delete();
        return response()->json(null, 204);
    }

  
    public function getloutdate(){
        //return response()->json(erpw_datesave::all(), 200);

        return 
        $tasks = erpw_datesave::with('users')->get();
        
        $assignedTo->users->lastname;

    }
     
    

public function update(Request $request, $id){
    $student = erpw_user::find($id);
    if($request->hasfile('image')) {
        $destination = 'public/'.$student->image;
        if(erpw_user::exists($destination))
        {
            erpw_user::delete($destination);
        }
        $image = $request->file('image');
        $extension = $image->getClientOriginalExtension();
        $filename = time(). '.' . $extension;
        $image->move('public/', $filename);
        $student->image = $filename;
    }
    $student->update();

}

public function adddescription(Request $request , $id)
{
        //  $user = Auth::id();
        // DB::table('erpw_users')
        //     ->update([
        //         'description' => $request->description
        //     ]);
        //     return response($user, 200);


            $team = erpw_user::find($id);
            if(is_null($team)) {
                return response()->json(['message' => 'Not Found'], 404);
            }
            $team->update($request->all());
            return response($team, 200);
            
 }

 public function getdescription($id){
    //$id = Auth::id();
    return
   $description = erpw_user::where('id',"=",$id)->pluck('description');

 }


 public function updateimage(Request $request ,$id)
{
  
    // $team = erpw_user::find($id);
    // if(is_null($team)) {
    //     return response()->json(['message' => 'Not Found'], 404);
    // }
    // $team->update($request->all());
    // return response($team, 200);
 return   $post = erpw_user::find($id);
                //  $completeFileName= $request->file('image')->getClientOriginalName();
                //          dd($completeFileName);
                
                // $data = $request->input('image');
                // $photo = $request->file('image')->getClientOriginalName();
                // $destination = base_path() . '/public/uploads';
                // $request->file('image')->move($destination, $photo);
               
                
                if($request->hasFile('avatar')){

                    $image=$request->file('avatar');
                    $filename=time() . '.' . $image->getClientOriginalExtension();
                    $location=public_path('images/' .$filename);
                    Image::make($image)->resize(300, 300)->save($location);
        
                    $leads->image=$filename;
        
        
                    $leads->save();
               }
               else{
        
        
                $leads->save();
               }
}


public function patchimage(Request $request , $id)
{
        $user = auth()->user();
      return  DB::table('erpw_users')
        ->where('id',"=",$id)
      

            ->update([
                'image'    => $request->image
            ]);
  }
    
    }