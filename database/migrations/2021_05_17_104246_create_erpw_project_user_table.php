<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErpwProjectUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erpw_project_users', function (Blueprint $table) {
            $table->increments('taskId');
            $table->integer('project_id')->index()->nullable();
            $table->integer('user_id')->index()->nullable();

            // $table->json('project_id');
            // $table->json('user_id');
           // $table->enum('roleenum', ['Developer', 'Techlead', 'Super_Admin']);
            $table->string('roleenum');
            $table->timestamps();
            $table->string('taskTitle', 191)->nullable();
			$table->string('taskMessage', 191)->nullable();
			$table->string('status', 191)->default('New');
			$table->date('createdOn')->nullable();
			$table->date('releasedate')->nullable();
			$table->date('completeddate')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
