<?php

namespace App\Http\Controllers;
use App\erpw_document;
use DB;
use App\erpw_user;
use Auth;
use Illuminate\Support\Str;
use File;
use Response;
use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

use Illuminate\Http\Request;

class documentController extends Controller
{
    
public function uploadimage(Request $request , $id)
{
  //dd($request->all());
         $user_image = erpw_user::where('id',"=",$id)->first();
   if ($request->hasFile('image'))
  {
        $image      = $request->file('image');
        $filename  = $image->getClientOriginalName();
        $extension = $image->getClientOriginalExtension();
        $picture   = date('His').'-'.$filename;
        $image->move(public_path('public/img'), $picture);
        //$image =  erpw_user::whereId('1')->first()->save();
       
        $user_image->image 	= $picture;
        $user_image->save();
        return response()->json(["message" => "Image Uploaded Succesfully"]);
  } 
  else
  {
        return response()->json(["message" => "Select image first."]);
  }
}


public function getimageuser($id){
    //$id = Auth::id();
    return
   $description = erpw_user::where('id',"=",$id)->pluck('image');

 }

 public function displayImage($id)

{

   $description = erpw_user::where('id',"=",$id)->pluck('image')->first();
  

    $path = public_path('public/img/' . $description);
    if (!File::exists($path)) {
        abort(404);
    }
    $image = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($image, 200);
    $response->header("Content-Type", $type);
    return $response;

}

public function displayAllmages()

{

  //  $description = erpw_user::all()->pluck('image');
  //  //return $description;
  //  foreach ($description as $memu) {
  //   // return $memu.'<br>';
  

  //   $path = public_path('img/' . $memu);
  //   if (!File::exists($path)) {
  //       abort(404);
  //   }
  //   $image = File::get($path);
  //   $type = File::mimeType($path);
  //   $response = Response::make($image, 200);
  //   $response->header("Content-Type", $type);
  //   return $response;
  //  }

  return $images = \File::allFiles(public_path('img'));
}

public function getLink()
{
    return erpw_document::with('user')->get();
}
public function deletelink(Request $request, $id) {
  $link = erpw_document::find($id);
  if(is_null($link)) {
      return response()->json(['message' => 'Not Found'], 404);
  }
  $link->delete();
  return response()->json(null, 204);
}
public function insertlink(Request $request) {
  $user = erpw_document::create([
     'link' => $request->link,
     'user_id' => $request->user_id
  ]);
      $user->save();
      $response['status'] = 1;
      $response['message'] = 'Information added Successfully';
      $response['code'] = 200;

return response()->json($response);
}
}