<?php

namespace App;
use App\erpw_user;
use Illuminate\Database\Eloquent\Model;

class erpw_datesave extends Model
{
    protected $fillable = [
       'lastsigninat', 'last_login_ip', 'last_logout','assignedTo',
    ];
    public function users(){
        return $this->belongsTo(erpw_user::class , 'assignedTo');
    }
   
}
