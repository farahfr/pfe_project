<?php

namespace App\Http\Controllers;

use App\erpw_events;
use Illuminate\Http\Request;
use Auth;

class eventController extends Controller
{
    public function getEvent()
    {
        return response()->json(erpw_events::all(), 200);
    }
    public function insertEvent(Request $request)
    {
        $test = erpw_events::create($request->all());
        return response($test, 201);
    }
    public function deleteEvent(Request $request, $id) {
        $evnt = erpw_events::find($id);
        if(is_null($evnt)) {
            return response()->json(['message' => 'Not Found'], 404);
        }
        $evnt->delete();
        return response()->json(null, 204);
    }
    public function getEventById($id)
    {
        $evnt = erpw_events::find($id);
        if (is_null($evnt)) {
            return response()->json(['message' => 'Ouups!'], 404);
        }
        return response()->json($evnt::find($id), 200);
    }
    public function updateEvent(Request $request, $id) {
        $evnt = erpw_events::find($id);
        if(is_null($evnt)) {
            return response()->json(['message' => 'Not Found'], 404);
        }
        $evnt->update($request->all());
        return response($evnt, 200);
    }


    public function eventadd(Request $request){
        //$event->userid = Auth::id();
        $event = new erpw_events;
        $user = auth()->user();
        $userid = Auth::id();

        $event->title = $request->title;
        $event->userid = $request->userid;
        $event->end = $request->end;
        $event->start = $request->start;
        $event->userid =auth()->user()->id;
     
        
        $event->save();
        $response['message'] = 'Event Added Successfully';
        $response['status'] = 1;
        $response['code'] = 200;

        return response()->json($response);
        
    }

    public function geteventbyuser($userid)
    {
        $user = auth()->user();
        return
        $data = erpw_events::where('userid',"=",$userid)->get();
    }
}
