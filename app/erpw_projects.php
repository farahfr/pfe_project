<?php

namespace App;
use App\erpw_teams;
use App\erpw_cras;
use App\erpw_task;
use App\erpw_user;
use App\erpw_project_user;
use Illuminate\Database\Eloquent\Model;

class erpw_projects extends Model
{

    
    public function cra(){
        return $this->belongsTo('App\erpw_cras');
    }
    // public function task(){
    //     return $this->belongsToMany('App\erpw_task');
    // }
    public function task() {
        return $this->belongsTo(erpw_project_user::class , 'project_id');
      }
      public function user() {
        return $this->belongsTo(erpw_user::class , 'assignedTo');
      }
    public function techlead() {
        return $this->belongsTo(erpw_user::class , 'assignedTo');
      }

    public $timestamps = false;
    protected $fillable = ['projectTitle', 'projectMessage',  'assignedTo', 'createdOn', 'releasedate', 'status', 'budget'];
    protected $primaryKey = 'projectId';




    public function tasks()
    {
        return $this->morphMany('App\task', 'taskassigned');
    }
}
