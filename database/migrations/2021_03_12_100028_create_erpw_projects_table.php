<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateErpwProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('erpw_projects', function(Blueprint $table)
		{
			$table->increments('projectId');
			$table->timestamps();
			$table->string('projectTitle', 191);
			$table->string('projectMessage', 191);
			$table->string('status', 191)->default('New');
			$table->date('createdOn');
			$table->date('releasedate');
			$table->decimal('budget')->nullable();
            $table->integer('assignedTo')->index()->nullable();

           // $table->integer('assignedTo')->unsigned()->index();
            //$table->unsignedInteger('team_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('projects');
	}

}
