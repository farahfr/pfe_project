<?php

namespace App\Http\Controllers;
use Illuminate\Validation\Rule;
use Validator;

use App\erpw_conge;
use App\erpw_user;
use Illuminate\Http\Request;
use App\Mail\CongeMail;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Mail;
use Auth;
use App\erpw_notification;
use App\Notifications\CongeNotification;
use Illuminate\Support\Facades\Notification;
class CongeController extends Controller
{
    public function getDays($user_id = null){
        return
        $ts = DB::table('erpw_conges')->selectRaw(DB::raw('nbr'))->where('user_id',"=",$user_id)->orderBy('created_at', 'DESC')->pluck('nbr')->first();
        
    }

    public function getConge()
    {
        return
        $tasks = erpw_conge::with('user')->get();
        $user_id->user->lastname;
    }

    public function updateConge(Request $request, $user_id) {
        $conge = erpw_conge::with('user')->find($user_id);
        if(is_null($conge)) {
            return response()->json(['message' => 'Not Found'], 404);
        }
        
        $conge->update([
            'status' =>($request->status),
            
    ]);
        
         if($request->status == 'Accepted'){
            $datedebut =DB::table('erpw_conges')->selectRaw(DB::raw('datedebut'))->where('user_id',"=",$conge->user_id)->orderBy('created_at', 'DESC')->pluck('datedebut')->first();
            $datefin =DB::table('erpw_conges')->selectRaw(DB::raw('datefin'))->where('user_id',"=",$conge->user_id)->orderBy('created_at', 'DESC')->pluck('datefin')->first();    
            $days = (strtotime($datefin) - strtotime($datedebut) )/ 86400;
            $ts =DB::table('erpw_conges')->orderBy('created_at', 'DESC')->select(DB::raw('nbr'))->where('user_id',"=",$conge->user_id)->pluck('nbr')->first();
          
        // $conge = erpw_conge::with('user')->find($user_id);
         
        $conge->orderBy('created_at', 'DESC')->where('user_id',"=",$conge->user_id)->orderBy('created_at', 'DESC')->limit(1)->update([
          'nbr' => $ts - ($days) - 1
            ]);
          }
       
       
        $test = DB::table('erpw_conges')->selectRaw(DB::raw('nbr'))->where('user_id',"=",$conge->user_id)->orderBy('created_at', 'DESC')->pluck('nbr')->first();
        //$data = array('status'=>$test );
        $mailusers =  erpw_conge::with('user')->selectRaw(DB::raw('user_id'))->where('user_id',"=",$conge->user_id)->first()->user->email;
        
        
        $data = array('status'=>$conge , 'statu'=>$test);
          
        //return dd($data);

        Mail::send('mail',$data, function($message) use ($mailusers){
        //$conge = erpw_conge::with('user')->find($user_id);
        //$mailusers =  erpw_conge::with('user')->selectRaw(DB::raw('user_id'))->where('user_id',"=",$conge->user_id)->first()->user->email;
            $message->to($mailusers)
            ->from($address = 'noreply@gmail.com', $name = 'ERP_Welyne')
            ->subject('Leave Request');
         });
         
        //return $conge;
        
    }

    public function getCongeById($user_id)
    {
        $user = auth()->user();
        return
        $member = erpw_conge::where('user_id',"=",$user_id)->get();
    }



    public function addConge(Request $request){
        $weekends = [];
        $user = auth()->user();
       // $user = Auth::user();
        $member = erpw_conge::where('user_id',"=",$request->user_id)->first();
        $nbr = DB::table('erpw_conges')->selectRaw(DB::raw('nbr'))->where('user_id',"=",$request->user_id)->orderBy('created_at', 'DESC')->pluck('nbr')->first();
      
    if($member)
    {
        


        $ts = DB::table('erpw_conges')->selectRaw(DB::raw('nbr'))->where('user_id',"=",$request->user_id)->orderBy('created_at', 'DESC')->pluck('nbr')->first();

        $conge = new erpw_conge;
        $user = auth()->user();
        // if(Carbon::now()->isWeekend()) {
        //     return 'hola';
        // }
        
        
        $days = (strtotime($request->datefin) - strtotime($request->datedebut) )/ 86400 ;
        $conge->datedebut = $request->datedebut;
        $conge->datefin = $request->datefin;
        //$conge->user_id = $request->user_id;
        $conge->user_id =$request->user_id;
        $conge->type = $request->type;
        $conge->description = $request->description;
        $conge->nbr =$ts;
       
        if($ts - $days <= 0){
            $response['status'] = 0;
            $response['message'] = 'Oups You have exceeded the number of days allowed';
            $response['code'] = 401;

            return response()->json($response);

            }
        $conge->save();
        $response['message'] = 'Day-off Request sent Successfully';
        $response['status'] = 1;
        $response['code'] = 200;

        
    }
    else{
        $user = auth()->user();
        $user_id = Auth::id();

        $conge = new erpw_conge;
        $days = (strtotime($request->datefin) - strtotime($request->datedebut) )/ 86400 ;

        $conge->datedebut = $request->datedebut;
        $conge->datefin = $request->datefin;
        $conge->user_id = $request->user_id;
        //$conge->user_id =$user->id;
       // $conge->user_id =auth()->user()->id;

        $conge->type = $request->type;
        $conge->description = $request->description;
        $conge->nbr ='22';
        if($days > '22'){
            
            $response['status'] = 0;
            $response['message'] = 'You have exceeded the number of days allowed';
            $response['code'] = 401;

            return response()->json($response);

            }
        $conge->save(); 
       
        $response['message'] = 'Day-off Request sent Successfully';
        $response['status'] = 1;
        $response['code'] = 200;
        

    }
    if($conge->save()){
        // $user = erpw_user::find($request->user_id);
         $user = erpw_user::where('role',"=",'Super_Admin')->get();
 
         //return $user;
         Notification::send($user , new CongeNotification($request->user_id));
         //Notification::findOrFail($request->user_id)->notify(new CongeNotification());
         }
    return response()->json($response);

}

public function deleterequest(Request $request, $id) {
    $conge = erpw_conge::find($id);
    if(is_null($conge)) {
        return response()->json(['message' => 'Not Found'], 404);
    }
    $conge->delete();
    return response()->json(null, 204);
}

public function updaterequest(Request $request , $id)
{

    
    $conge = erpw_conge::find($id);
    $nbr = DB::table('erpw_conges')->selectRaw(DB::raw('nbr'))->where('id',"=",$id)->pluck('nbr')->first();
    $days = (strtotime($request->datefin) - strtotime($request->datedebut) )/ 86400 ;
    if($validator)    {
    $conge->update($request->all());
    }

    $response['message'] = 'Leave Request updated Successfully';
    $response['status'] = 1;
    $response['code'] = 200;

    return response()->json($conge);


 }

    }

