<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class erpw_role_user extends Model
{    public $timestamps = false;

    protected $fillable = [
        'role_id', 'user_id', 
    ];
   // protected $casts = [ 'role_id' => 'array' ];
   protected $casts = [
    'role_id' => 'json' ,
    'user_id' => 'json'

];
}
