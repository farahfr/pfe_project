<?php

namespace App;
use App\erpw_user;
use Illuminate\Database\Eloquent\Model;

class erpw_document extends Model
{
  public function user() {
    return $this->belongsTo(erpw_user::class , 'user_id');
  }
    public $timestamps = true;
    protected $fillable = ['user_id', 'link'];
}
