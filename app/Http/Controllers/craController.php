<?php

namespace App\Http\Controllers;

use App\erpw_cras;
use Illuminate\Http\Request;
use App\erpw_project_user;
use Carbon\Carbon;
use Auth;
use DB;
use DateTime;

class craController extends Controller
{
    public function getCra()
    {
        return response()->json(erpw_cras::all(), 200);
    }
    public function insertCra(Request $request)
    {
        $test = erpw_cras::create($request->all());
        return response($test, 201);
    }
    public function deleteCra(Request $request, $id) {
        $team = erpw_cras::find($id);
        if(is_null($team)) {
            return response()->json(['message' => 'Not Found'], 404);
        }
        $team->delete();
        return response()->json(null, 204);
    }
    public function getCraById($userid)
    {
        $user = auth()->user();
        return
        $member = erpw_cras::with('taskid')->with('projectid')->where('userid',"=",$userid)->get();
    }
    public function updateCra(Request $request, $id) {
        $team = erpw_cras::find($id);
        if(is_null($team)) {
            return response()->json(['message' => 'Not Found'], 404);
        }
        $team->update($request->all());
        return response($team, 200);
    }
    public function gettaskproject(Request $request, $projectid , $userid)
    {
        return
        $project = erpw_project_user::with('project')->where('project_id',"=",$projectid)->where('user_id',"=",$userid)->pluck('taskTitle');
    }
    public function projectlistperuser(Request $request, $userid)
    {
        return
        $project = erpw_project_user::with('project')->where('user_id',"=",$userid)->select('project_id')->distinct()->get();
    }

    public function week(Request $request , $id){
            return 
            $return = erpw_cras::with('projectid')->all()->where('userid',"=",$id)->groupBy(function($date) {
            $created_at = Carbon::parse($date->days);
            $start = $created_at->startOfWeek()->format('d-m-Y');
            $end = $created_at->endOfWeek()->format('d-m-Y');
            
            return "{$start} - {$end}";
        });
     
    }


    // public function week(Request $request){
    //     return   $return = erpw_cras::all()->groupBy(function($date) {
    //            $created_at = Carbon::parse($date->days);
    //            $start = $created_at->startOfWeek()->format('d-m-Y');
    //            $end = $created_at->endOfWeek()->format('d-m-Y');
               
    //            return "{$start} - {$end}";
    //        });
       
    //    }

    public function thisweek(Request $request , $id){
        return
        erpw_cras::with('projectid')->where('days', '>', Carbon::now()->startOfWeek())
        ->where('days', '<', Carbon::now()->endOfWeek())->where('userid',"=",$id)
        ->get();
    }


    public function addcra(Request $request){
        //$event->userid = Auth::id();
        $cra = new erpw_cras;
        $user = auth()->user();
        $userid = Auth::id();

        $cra->projectid = $request->projectid;
        $cra->userid = $request->userid;
        $cra->taskid = $request->taskid;
        $cra->status = $request->status;
        $cra->days = $request->days;
        $cra->description = $request->description;
        //$cra->userid =auth()->user()->id;
     
        
        $cra->save();
        $response['message'] = 'Cra Added Successfully';
        $response['status'] = 1;
        $response['code'] = 200;

        return response()->json($response);
        
    }

    public function geteventbyuser($userid)
    {
        $user = auth()->user();
        return
        $data = erpw_events::where('userid',"=",$userid)->get();
    }

   public function databyweek(Request $request , $id){
        return   
         $return = erpw_cras::all()->where('userid',"=", $id)->groupBy(function($date) {
               $created_at = Carbon::parse($date->days);
               $start = $created_at->startOfWeek()->format('d-m-Y');
               $end = $created_at->endOfWeek()->format('d-m-Y');
               
               return "{$start} - {$end}";
           });
       
       }

       public function addfunction(Request $request){
        $cra = new erpw_cras;
        $user = auth()->user();
        $userid = Auth::id();
        
       // $now = Carbon::now();
        //$now = Carbon::createFromFormat('d-m-Y', $request->startofweek)->format('d/m/Y');
        $cra->projectid = $request->projectid;
        $cra->userid = $request->userid;
        $cra->taskid = $request->taskid;
        $cra->status = $request->status;
        $cra->days = $request->days;
        $cra->description = $request->description;
        $cra->startofweek = Carbon::parse($request->days)->startOfWeek()->format('d-m-Y ');
        $cra->endofweek =  Carbon::parse($request->days)->endOfWeek()->format('d-m-Y ');
        $cra->save();
        $response['message'] = 'Activity Report has been saved Successfully';
        $response['status'] = 1;
        $response['code'] = 200;

        return response()->json($response);
       }
       public function getUserById($id)
       {
           $team = erpw_user::find($id);
           if (is_null($team)) {
               return response()->json(['message' => 'Ouups!'], 404);
           }
           return response()->json($team::find($id), 200);
       }

       public function groupcrabyweek(Request $request , $id){
        return   
         $return = DB::table('erpw_cras')->select('projectid','taskid','status', 'startofweek','endofweek', DB::raw('count(*) as total'))
         ->where('userid',"=", $id)->groupBy('startofweek')->get();
       
// $return = erpw_cras::all()->where('userid',"=", $id);
// return $return->count();
       
}
        public function getlist($id){
            return DB::table('erpw_cras')->select('startofweek' , 'endofweek')->where('userid',"=",$id)->distinct()->get();
        }


        public function userweek(Request $request , $id, $startofweek){
            return   
             $return = erpw_cras::with('projectid')->where('userid',"=", $id)->where('startofweek',"=", $startofweek)->get();
           
           }

           public function weeknumber(){
            $ddate = "2021-01-08";
            $date = new DateTime($ddate);
            $week = $date->format("W");
            echo "Weeknummer: $week";
           }



           public function testlist(Request $request, $userid)
           {
               return
               $project = erpw_project_user::where('user_id',"=",$userid)->select('project_id')->distinct()->get();
           }
}
