<?php

namespace App;
use App\erpw_user;

use Illuminate\Database\Eloquent\Model;

class erpw_conge extends Model
{
    // public function Users(){
    //     $this->hasMany('App/erpw_user', 'foreign_key');
    // }

    public $timestamps = true;
    protected $fillable = ['datedebut', 'datefin',  'user_id', 'type', 'status' , 'description'];
    protected $dates = [
        'datedebut',
        'datefin'
      ];

    public function user(){
        return $this->belongsTo(erpw_user::class , 'user_id');
    }
   
}
