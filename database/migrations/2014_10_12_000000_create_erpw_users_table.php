<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErpwUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erpw_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname', 191);
            $table->date('birthdate');
            $table->string('phonenumber', 191);
            $table->string('experience')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('image', 191)->default('defaultimage.jpeg');
 			$table->string('role', 191)->default('Guest');
 			$table->string('default', 191)->default('User');
            $table->string('description')->nullable();
            $table->string('data')->nullable();
            $table->boolean('isTechlead')->nullable();
            //$table->enum('roleenum', ['Techlead', 'Developer', 'Super_Admin']);


            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
