<?php

namespace App;
use App\erpw_projects;
use App\erpw_task;
use App\erpw_project_user;
use Illuminate\Database\Eloquent\Model;

class erpw_cras extends Model
{
    public function user() {
        return $this->belongsTo(erpw_user::class , 'userid');
      }
    public function projectid() {
        return $this->belongsTo(erpw_projects::class , 'projectid');
      }
      public function taskid() {
        return $this->belongsTo(erpw_project_user::class , 'taskid');
      }
    public $timestamps = false;
    protected $fillable = ['status', 'date',  'userid', 'projectid', 'taskid' ,'days', 'description', 'startofweek', 'endofweek'];
    // protected $dates = [
    //   'days'
    // ];
  }
