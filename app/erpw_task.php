<?php

namespace App;
use App\erpw_teams;
use App\erpw_cras ;
use App\erpw_projects;
use Illuminate\Database\Eloquent\Model;

class erpw_task extends Model
{

    public function User(){
        return $this->hasMany('App\erpw_user', 'assignedTo' , 'id');
    }
    public function cra(){
        return $this->belongsTo('App\erpw_cras');
    }
    public function projects(){
        return $this->belongsToMany('App\erpw_projects');
    }
    public $timestamps = false;
    protected $fillable = ['taskTitle', 'taskMessage',  'assignedTo', 'createdOn', 'releasedate', 'status', 'taskassigned_id', 'taskassigned_type'];
    protected $primaryKey = 'taskId';

    public function taskassigned()
    {
        return $this->morphTo();
    }
}
