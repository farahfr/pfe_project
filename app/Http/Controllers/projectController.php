<?php

namespace App\Http\Controllers;
use DB;

use App\erpw_conge;
use App\erpw_projects;
use Illuminate\Http\Request;
use App\erpw_project_user;
use App\erpw_user;
use App\Notifications\ProjectNotification;
use App\Notifications\TechleadNotification;
use Illuminate\Support\Facades\Notification;
class projectController extends Controller
{
    public function getProject()
    {
        return erpw_projects::with('techlead')->get();
    }
   

    public function getOnlyName(){
        return DB::table('erpw_projects')->pluck('projectTitle');

    }


    public function addProject(Request $request)
    {
    //    // $user = erpw_user::all();
    //     $test = erpw_projects::create($request->all());
    //   //  Notification::send($user , new ProjectNotification($request->projectTitle));
    //     return response($test, 201);

    $project = new erpw_projects;
 
    $project->projectTitle = $request->projectTitle;
    $project->projectMessage = $request->projectMessage;
    $project->assignedTo=$request->assignedTo;
    $project->createdOn = $request->createdOn;
    $project->releasedate = $request->releasedate;
    $project->budget = $request->budget;
    
    $user = erpw_user::where('id',"=",$request->assignedTo)->update([
            'isTechlead'      => '1',
        ]);
    
      
        // $ts = erpw_projects::where('assignedTo',"=",$request->assignedTo)->get();
        // //return $ts;
        // Notification::send($ts , new TechleadNotification($request->assignedTo));
        
    $project->save();
    $response['message'] = 'New Project Added Successfully';
    $response['status'] = 1;
    $response['code'] = 200;

 
    

    }
    public function deleteProject(Request $request, $id) {
        $team = erpw_projects::find($id);
        if(is_null($team)) {
            return response()->json(['message' => 'Not Found'], 404);
        }
        $team->delete();
        return response()->json(null, 204);
    }
    public function getProjectById($id)
    {
        $team = erpw_projects::find($id);
        if (is_null($team)) {
            return response()->json(['message' => 'Ouups!'], 404);
        }
        return response()->json($team::find($id), 200);
    }
    public function updateProject(Request $request, $id) {
        $team = erpw_projects::find($id);
        if(is_null($team)) {
            return response()->json(['message' => 'Not Found'], 404);
        }
        $team->update($request->all());
        return response($team, 200);
    }


    public function getUserByProject($id){
        $test = erpw_project_user::where('user_id',"=",$id)->get();
        if(is_null($test)){
            return response()->json(['message' => 'Oups'], 404);
        }
        return response()->json($test,200);
       }

    public function getSum(){
  
    return DB::table('erpw_projects')
                 ->select(DB::raw('count(*) as projectId'))
                 //->where('cat_id', $id)
       
                 ->count();

   
    
    }   
    public function getprojectperuser($id){
        return DB::table('erpw_project_users')
                 ->select(DB::raw('count(*) as projectId'))
                 ->where('user_id', $id)
                 ->count();
    }
    
    public function getstatus(){
        return DB::table('erpw_users')->pluck('id');

    }
    public function getprojecttitle(){
        return DB::table('erpw_projects')->pluck('projectTitle');

    }
    public function getprojectstatus(){
        return DB::table('erpw_projects')->pluck('status');

    }

    public function getstasproject(){
        $project=  erpw_projects::get();
        
        $taskstatus=  DB::table('erpw_projects')->selectRaw('projectId')->where('status',"=", 'New')->count();
        $taskstatus1=  DB::table('erpw_projects')->selectRaw('projectId')->where('status',"=",'In-Process')->count();
        $taskstatus2=  DB::table('erpw_projects')->selectRaw('projectId')->where('status',"=",'Pending')->count();
        $taskstatus3=  DB::table('erpw_projects')->selectRaw('projectId')->where('status',"=",'Completed')->count();
        // if(($taskstatus == 0 ) && ($taskstatus1 == 0) && ($taskstatus2 == 0) && ($taskstatus3== 0) ) {
        //     return response()->json(array(['message' => 'There Is No Data!']), 404);
        // }
    return response()->json(array($taskstatus,$taskstatus1,$taskstatus2,$taskstatus3));
}
}
