<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class erpw_events extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'start',  'end', 'primary', 'secondary' , 'userid'];
    // protected $dateFormat = 'Y-m-d H:i:s';

    // protected $casts = [
    //     'start' => 'datetime:Y-m-d H:i:s',
    //     'end' => 'datetime:Y-m-d H:i:s',
    // ];


}
