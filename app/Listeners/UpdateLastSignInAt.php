<?php

namespace App\Listeners;

use Carbon\Carbon;
use http\Env\Request;
use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateLastSignInAt
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param Login $event
     * @return void
     */
    public function handle(login $event)
    {
        $user = $event->user;
        DB::table('erpw_users')
            ->where('id', $user->id)
            ->update([
                'last_logout'    => date('Y-m-d H:i:s'),
            ]);


    }
}
